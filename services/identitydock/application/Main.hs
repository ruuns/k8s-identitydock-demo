module Main where

import Web.Spock
import Web.Spock.Config
import qualified Network.Wai.Middleware.RequestLogger as Wai
import qualified Network.Wai.Handler.Warp as Warp
import qualified Identitydock.App as Identitydock

main :: IO ()
main = do
  cfg <- defaultSpockCfg () PCNoDatabase Identitydock.getInitialState
  app <- spockAsApp (spock cfg Identitydock.getApp)
  Warp.runSettings (Warp.setPort 1900 Warp.defaultSettings) (Wai.logStdout app)
