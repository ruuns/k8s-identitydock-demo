module Identitydock.App
   ( getApp
   , getInitialState )
where

import Web.Spock 
import Data.Text.Lazy ( toStrict )
import Text.Blaze.Html5 ( (!) )
import Data.Text ( Text )
import Data.String ( fromString )
import qualified Identitydock.Constants as Constants
import qualified Data.Text as Text
import qualified Data.Text.Encoding as Text
import qualified Text.Blaze.Html.Renderer.Text as H
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
import qualified Crypto.Hash.SHA256 as SHA256
import qualified Data.ByteString.Base16 as Base16 
import qualified Network.HTTP.Simple as Http

type IdentityState = ()
type IdentitySession = () 
type IdentityConn = ()
type IdentityDock a = SpockM IdentityConn IdentitySession IdentityState a
type IdentityAction a = ActionT (WebStateM IdentityConn IdentitySession IdentityState) a

html' :: H.Html -> IdentityAction ()
html' = html . toStrict . H.renderHtml

toHtml :: Text -> H.Html
toHtml = H.toHtml

getInitialState :: IdentityState
getInitialState = ()

getHealth :: IdentityAction ()
getHealth = text "OKAY" 

hashName :: Text -> Text
hashName = Text.decodeUtf8 . Base16.encode . SHA256.hash . Text.encodeUtf8

getMonsterUri :: Text -> Text
getMonsterUri = Text.append "/monster/" . hashName

getMonsterRequest :: Text -> Http.Request
getMonsterRequest txt = 
   let req = Text.concat ["http://", Constants.dnmonsterEndpoint, "/monster/", txt, "?size=80"] in
   Http.parseRequest_ (Text.unpack req)

writeHtmlForm :: Text -> IdentityAction ()
writeHtmlForm name = html' $ 
   H.docTypeHtml $ do 
      H.head $ 
         H.title "Identitydock"
      H.body $ do
         H.form ! A.method "POST" $ do
            toHtml "Hallo"
            H.input ! A.type_ "text" ! A.name "name" ! A.value (fromString (Text.unpack name))
            H.input ! A.type_ "submit" ! A.value "Sent"
         H.p $ do
            toHtml "You are looking like:" 
            H.img ! A.src (fromString (Text.unpack (getMonsterUri name)))

getRoot :: IdentityAction ()
getRoot = do 
   name <- param "name" 
   case name of 
      Just name -> writeHtmlForm name
      Nothing   -> writeHtmlForm Constants.defaultName

getMonster :: Text -> IdentityAction ()
getMonster txt = requestMonster txt

requestMonster :: Text -> IdentityAction ()
requestMonster txt = 
   Http.httpBS (getMonsterRequest txt) >>= bytes . Http.getResponseBody

getApp :: IdentityDock ()
getApp = do
   get "health" getHealth
   get ("monster" <//> var) getMonster
   get root getRoot
   post root getRoot

