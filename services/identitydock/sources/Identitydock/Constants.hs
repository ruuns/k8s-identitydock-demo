module Identitydock.Constants
   ( defaultName 
   , dnmonsterEndpoint
   , redisEndpoint )
where 

import Data.Text ( Text )

defaultName :: Text
defaultName = "Jack Daniels"

dnmonsterEndpoint :: Text 
dnmonsterEndpoint = "dnmonster:8080"

redisEndpoint :: Text
redisEndpoint = "rediscache:6379"


