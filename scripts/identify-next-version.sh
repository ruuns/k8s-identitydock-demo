#!/usr/bin/env bash

# 	Collect all new commits in DIR1 DIR2 ... relative to the latest TAGNAME 
# 	and determine if we have major, minor or patch release
#
# Params:
# 	$1 TAGNAME - collect commits from this tag to HEAD
#   $2 DIR1    - First directory where git log should be executed
# 	$n ...     - Next directory 
function getNextVersionTag {
	local TAG=$1
	shift
	COMMITS=$(git log $TAG..HEAD --oneline $1)

	while (( "$#" )); do
		COMMITS+=$(git log $TAG..HEAD --oneline $1)
		shift
	done
	
	FT_BUMP=$(echo $COMMITS | awk '{ print $2 }' | grep "^FT" | wc -l)
	BUG_BUMP=$(echo $COMMITS | awk '{ print $2 }' | grep "^BUG" | wc -l)
	MAJ_BUMP=$(echo $COMMITS | awk '{ print $2 }' | grep "^MAJ" | wc -l)

	if [ $MAJ_BUMP -gt 0 ]; then
		echo "MAJ"
	elif [ $FT_BUMP -gt 0 ]; then
		echo "FT"
	elif [ $BUG_BUMP -gt 0 ]; then
		echo "BUG"
	else
		echo "NONE"
	fi
}

if [ $# -lt 1 ]; then
	echo "ERROR: No service directories given" >&2
	exit 1
fi

SRV_DIR=$(basename $(readlink -f $1))
TAG=$(git describe --match=$SRV_DIR-* --abbrev=0)

if ! (echo $TAG | grep -q -P "\w+\-\d+\.\d+\.\d+$"); then
	echo "ERROR: $TAG has invalid tag format. Must be something like TAGNAME-MAJOR.MINOR.PATCH"
	exit 1
fi

TAGNAME=$(echo $TAG | cut -d '-' -f1)
VERSION=$(echo $TAG | cut -d '-' -f2)
MAJOR_VERSION=$(echo $VERSION | cut -d '.' -f1)
MINOR_VERSION=$(echo $VERSION | cut -d '.' -f2)
PATCH_VERSION=$(echo $VERSION | cut -d '.' -f3)

case "$(getNextVersionTag $TAG $1)" in
	MAJ)
		echo "$TAGNAME-$((MAJOR_VERSION + 1)).$MINOR_VERSION.$PATCH_VERSION"
		;;
	FT)
		echo "$TAGNAME-$MAJOR_VERSION.$((MINOR_VERSION + 1)).$PATCH_VERSION"
		;;
	BUG)
		echo "$TAGNAME-$MAJOR_VERSION.$MINOR_VERSION.$((PATCH_VERSION + 1))"
		;;
	NONE)
		echo "$TAGNAME-$MAJOR_VERSION.$MINOR_VERSION.$PATCH_VERSION"
		;;
esac

