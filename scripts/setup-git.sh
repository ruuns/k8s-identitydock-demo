#!/usr/bin/env bash
ROOT_DIR=$(git rev-parse --show-toplevel)

git config core.hooksPath $ROOT_DIR/scripts/githooks
